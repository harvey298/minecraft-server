.PHONY: all server plugins push

all: start

# Start the server
start:
	java -Xms2G -Xmx2G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs/ -Daikars.new.flags=true -jar server.jar nogui

# Update server
server:
	rm -rf server.jar
	curl https://api.pl3x.net/v2/purpur/1.17.1/latest/download -o server.jar
	rm -rf cache

# Update plugins
plugins:
	rm -rf plugins/*.jar
	curl https://ci.pl3x.net/job/Pl3xMap/lastSuccessfulBuild/artifact/plugin/build/libs/Pl3xMap-1.0.0-BETA-158.jar -o plugins/plexmap.jar
	curl https://ci.opencollab.dev/job/GeyserMC/job/Floodgate/job/master/lastSuccessfulBuild/artifact/spigot/target/floodgate-spigot.jar -o plugins/floodgate-spigot.jar
	curl https://ci.opencollab.dev//job/GeyserMC/job/Geyser/job/master/lastSuccessfulBuild/artifact/bootstrap/spigot/target/Geyser-Spigot.jar -o plugins/geyser-spigot.jar
	wget https://github.com/ViaVersion/ViaVersion/releases/download/4.0.1/ViaVersion-4.0.1.jar -O plugins/viaversion.jar
	wget https://github.com/plasmoapp/plasmo-voice/releases/download/1.0.5-spigot/plasmovoice-server-1.0.5.jar -O plugins/plasmovoice.jar
	wget https://github.com/PlayPro/CoreProtect/releases/download/v20.1/CoreProtect-20.1.jar -O plugins/coreprotect.jar

# Push to Git repository and clear CoreProtect cache
push:
	git add .
	git commit -m "Automated push via Makefile"
	git push -u origin main
